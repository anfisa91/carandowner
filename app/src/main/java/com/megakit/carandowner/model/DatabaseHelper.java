/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.megakit.carandowner.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.megakit.carandowner.application.CarAndOwnerApp;
import com.megakit.carandowner.model.dao.CarDao;
import com.megakit.carandowner.model.dao.OwnerDao;
import com.megakit.carandowner.model.entities.Car;
import com.megakit.carandowner.model.entities.Owner;

import java.sql.SQLException;

/**
 * Created by Anfisa on 10.08.2017.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String TAG = DatabaseHelper.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;
    private static DatabaseHelper databaseHelper;
    private static CarDao carDao = null;
    private static OwnerDao ownerDao = null;

    //Array of project database models.
    private static final Class[] DB_ENTITIES = {
            Car.class,
            Owner.class
    };

    //Created database file with app package name.
    private DatabaseHelper(Context context) {
        super(context, context.getApplicationContext().getPackageName() + ".db", null, DATABASE_VERSION);
    }

    public static DatabaseHelper get() {
        if (databaseHelper == null) {
            databaseHelper = new DatabaseHelper(CarAndOwnerApp.instance);
        }
        return databaseHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        for (Class entity : DB_ENTITIES) {
            try {
                TableUtils.createTable(connectionSource, entity);
            } catch (SQLException e) {
                Log.e(TAG, "error creating DB " + getDatabaseName());
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        for (Class entity : DB_ENTITIES) {
            try {
                TableUtils.dropTable(connectionSource, entity, true);
            } catch (SQLException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        onCreate(database, connectionSource);
    }

    @Override
    public void close() {
        super.close();
    }

    public CarDao getCarDao() {
        if (carDao == null) {
            try {
                carDao = new CarDao(getConnectionSource(), Car.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return carDao;
    }

    public OwnerDao getOwnerDao() {
        if (ownerDao == null) {
            try {
                ownerDao = new OwnerDao(getConnectionSource(), Owner.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return ownerDao;
    }

    public void releaseHelper() {
        OpenHelperManager.releaseHelper();
        databaseHelper = null;
    }
}
