/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.megakit.carandowner.model.entities;

import android.provider.BaseColumns;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Anfisa on 10.08.2017.
 */
@DatabaseTable(tableName = Car.TABLE_NAME)
public class Car {
    public static final String TABLE_NAME = "cars";
    public static final String ID_COLUMN = BaseColumns._ID;
    public static final String CAR_BODY_TYPE_COLUMN = "bodyType";
    public static final String CAR_MODEL_COLUMN = "carModel";
    public static final String CAR_BRAND_COLUMN = "carBrand";
    public static final String CAR_OWNER_COLUMN = "carOwner";

    //Id field will be generated automatically by OrmLite.
    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true, columnName = ID_COLUMN)
    private int id;

    @DatabaseField(columnName = CAR_BODY_TYPE_COLUMN)
    private String carBodyType;

    @DatabaseField(columnName = CAR_MODEL_COLUMN)
    private String carModel;

    @DatabaseField(columnName = CAR_BRAND_COLUMN)
    private String carBrand;

    //Corresponding Owner object
    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = CAR_OWNER_COLUMN)
    private Owner carOwner;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCarBodyType() {
        return carBodyType;
    }

    public void setCarBodyType(String carBodyType) {
        this.carBodyType = carBodyType;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public Owner getCarOwner() {
        return carOwner;
    }

    public void setCarOwner(Owner carOwner) {
        this.carOwner = carOwner;
    }

    //Override equals and hashCode for managing object logic.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        return id == car.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
