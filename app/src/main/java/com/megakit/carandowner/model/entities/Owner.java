/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.megakit.carandowner.model.entities;

import android.provider.BaseColumns;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Anfisa on 10.08.2017.
 */
@DatabaseTable(tableName = Owner.TABLE_NAME)
public class Owner {
    public static final String TABLE_NAME = "owner";
    public static final String ID_COLUMN = BaseColumns._ID;
    public static final String OWNER_FIRST_NAME_COLUMN = "ownerFirstName";
    public static final String OWNER_SECOND_NAME_COLUMN = "ownerSecondName";
    public static final String REGISTRATION_NUMBER_COLUMN = "registrationNumber";

    //Id field will be generated automatically by OrmLite.
    @DatabaseField(generatedId = true, columnName = ID_COLUMN)
    private int id;

    @DatabaseField(columnName = OWNER_FIRST_NAME_COLUMN)
    private String firstName;

    @DatabaseField(columnName = OWNER_SECOND_NAME_COLUMN)
    private String secondName;

    @DatabaseField(columnName = REGISTRATION_NUMBER_COLUMN)
    private String registrationNumber;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
}
