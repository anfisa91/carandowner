/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.megakit.carandowner.model.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.megakit.carandowner.model.entities.Owner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anfisa on 10.08.2017.
 */

public class OwnerDao extends BaseDaoImpl<Owner, Integer> {

    public OwnerDao(ConnectionSource connectionSource, Class<Owner> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    //Save single object to database.
    public void save(Owner owner) {
        try {
            createOrUpdate(owner);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Save collection of objects to database.
    public void saveAll(List<Owner> owners) {
        try {
            callBatchTasks(() -> {
                for (Owner owner : owners) {
                    // save our car object
                    save(owner);
                }
                return null;
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Get collection of objects from database.
    public List<Owner> getAll() throws SQLException {
        try {
            return queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    //Get owner object from database by id.
    public Owner getById(int ownerId) {
        try {
            return queryForId(ownerId);
        } catch (SQLException e) {
            e.printStackTrace();
            return new Owner();
        }
    }
}
