/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.megakit.carandowner.model.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.megakit.carandowner.model.DatabaseHelper;
import com.megakit.carandowner.model.entities.Car;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anfisa on 10.08.2017.
 */

public class CarDao extends BaseDaoImpl<Car, Integer> {

    public CarDao(ConnectionSource connectionSource, Class<Car> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    //Save single object to database.
    public void save(Car car) {
        try {
            if (car.getCarOwner() != null) {
                DatabaseHelper.get().getOwnerDao().save(car.getCarOwner());
            }
            createOrUpdate(car);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Save collection of objects to database.
    public void saveAll(List<Car> cars) {
        try {
            callBatchTasks(() -> {
                for (Car car : cars) {
                    // save our car object
                    save(car);
                }
                return null;
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Get collection of objects from database.
    public List<Car> getAll() {
        try {
            return queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    //Get car object from database by id.
    public Car getById(int carId) {
        try {
            return queryForId(carId);
        } catch (SQLException e) {
            e.printStackTrace();
            return new Car();
        }
    }

}
