/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.megakit.carandowner.ui;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;


/**
 * Created by Anfisa on 11.08.2017.
 */

public class BaseFragment extends Fragment {

    private BaseActivity activity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            this.activity = (BaseActivity) context;
        }
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            this.activity = (BaseActivity) context;
        }
    }

    //get activity fragments stack object
    protected FragmentsStack getFragmentStack() {
        return activity.getFragmentsStack();
    }
}
