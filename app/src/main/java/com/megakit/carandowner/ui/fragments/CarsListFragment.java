/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.megakit.carandowner.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.megakit.carandowner.R;
import com.megakit.carandowner.adapters.CarsAdapter;
import com.megakit.carandowner.model.entities.Car;
import com.megakit.carandowner.network.RestClient;
import com.megakit.carandowner.ui.BaseFragment;
import com.megakit.carandowner.ui.activities.MainActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Anfisa on 11.08.2017.
 */

public class CarsListFragment extends BaseFragment implements CarsAdapter.OnItemClickListener {

    @BindView(R.id.recycler)
    RecyclerView carsRecyclerView;
    @BindView(R.id.create_car_fab)
    FloatingActionButton createCarBtn;

    private Disposable carsListSubscription;
    private Disposable carIdSubscription;
    private ArrayList<Car> carsList = new ArrayList<>();
    private CarsAdapter carsAdapter;

    public static CarsListFragment newInstance() {
        CarsListFragment fragment = new CarsListFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        carsAdapter = new CarsAdapter(carsList);
        carsAdapter.setListener(this);
        getAllCarsRequest();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cars_list, container, false);
        ButterKnife.bind(this, view);
        createCarBtn.setOnClickListener(view1 -> getFragmentStack().push(CreateCarFragment.newInstance(-1)));
        carsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        carsRecyclerView.setAdapter(carsAdapter);
        ((MainActivity) getActivity()).changeToolbarTitle(getString(R.string.app_name));
        return view;
    }

    @Override
    public void onItemClicked(int position) {
        getFragmentStack().push(CreateCarFragment.newInstance(carsList.get(position).getId()));
    }

    @Override
    public void onDeleteItemClicked(int position) {
        deleteCarRequest(carsList.get(position).getId(), carsList.get(position).getCarOwner().getId());
    }

    //Get cars list response and update adapter
    private void getAllCarsRequest() {
        Observable<List<Car>> carsListData = RestClient.get().getCarsListData();
        carsListSubscription = carsListData
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cars -> displayCars((ArrayList<Car>) cars));
    }

    //Delete specific car and owner
    private void deleteCarRequest(Integer carId, Integer ownerId) {
        Observable<Integer> observable = RestClient.get().deleteCarId(carId, ownerId);
        carIdSubscription = observable
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::deleteCarOfList);
    }

    //Update list adapter
    private void displayCars(ArrayList<Car> carArrayList) {
        carsList.clear();
        carsList.addAll(carArrayList);
        carsAdapter.notifyDataSetChanged();
    }

    //Update specific car in list
    public void updateCar(Car car) {
        if (carsList.contains(car)) {
            carsList.set(carsList.indexOf(car), car);
        } else {
            carsList.add(car);
        }
        carsAdapter.notifyDataSetChanged();
    }


    public void deleteCarOfList(Integer id) {
        Car carToRemove = new Car();
        carToRemove.setId(id);
        carsList.remove(carToRemove);
        carsAdapter.notifyDataSetChanged();
    }

    //Dispose to avoid memory leaks
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (carsListSubscription != null && !carsListSubscription.isDisposed()) {
            carsListSubscription.dispose();
        }
        if (carIdSubscription != null && !carIdSubscription.isDisposed()) {
            carIdSubscription.dispose();
        }
    }
}
