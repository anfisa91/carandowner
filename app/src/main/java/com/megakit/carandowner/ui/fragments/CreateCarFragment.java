/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.megakit.carandowner.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.megakit.carandowner.R;
import com.megakit.carandowner.model.DatabaseHelper;
import com.megakit.carandowner.model.entities.Car;
import com.megakit.carandowner.model.entities.Owner;
import com.megakit.carandowner.network.RestClient;
import com.megakit.carandowner.ui.BaseFragment;
import com.megakit.carandowner.ui.activities.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Anfisa on 11.08.2017.
 */

public class CreateCarFragment extends BaseFragment {

    @BindView(R.id.car_brand_et)
    EditText carBrandEt;
    @BindView(R.id.car_model_et)
    EditText carModelEt;
    @BindView(R.id.car_body_type_et)
    EditText carBodyTypeEt;
    @BindView(R.id.first_name_et)
    EditText ownerFirstNameEt;
    @BindView(R.id.last_name_et)
    EditText ownerLastNameEt;
    @BindView(R.id.registration_number_et)
    EditText ownerRegistrationNumber;
    @BindView(R.id.save_btn)
    Button saveBtn;

    private final static int MIN_LENDTH = 2;
    private Disposable carSubscription;
    private OnCarSavedListener onCarSavedListener;
    private static final String CAR_ID = "car_id";
    private int carId;
    private Car carFromDB = new Car();

    public static CreateCarFragment newInstance(int carId) {
        CreateCarFragment fragment = new CreateCarFragment();
        Bundle args = new Bundle();
        args.putSerializable(CAR_ID, carId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_car, container, false);
        ButterKnife.bind(this, view);
        Bundle bundle = this.getArguments();
        ((MainActivity) getActivity()).changeToolbarTitle(getResources().getString(R.string.create_car));
        if (bundle != null) {
            carId = (int) bundle.get(CAR_ID);
        }
        //Check for create or edit car
        if (carId > -1) {
            ((MainActivity) getActivity()).changeToolbarTitle(getResources().getString(R.string.edit_car));
            carFromDB = DatabaseHelper.get().getCarDao().getById(carId);
        }
        if (carFromDB != null) {
            carBrandEt.setText(carFromDB.getCarBrand());
            carModelEt.setText(carFromDB.getCarModel());
            carBodyTypeEt.setText(carFromDB.getCarBodyType());

            if (carFromDB.getCarOwner() != null) {
                ownerFirstNameEt.setText(carFromDB.getCarOwner().getFirstName());
                ownerLastNameEt.setText(carFromDB.getCarOwner().getSecondName());
                ownerRegistrationNumber.setText(carFromDB.getCarOwner().getRegistrationNumber());
            }
        }
        saveBtn.setOnClickListener(view1 -> {
            if (valid()) {
                Car car = new Car();
                Owner owner = new Owner();
                owner.setFirstName(ownerFirstNameEt.getText().toString());
                owner.setSecondName(ownerLastNameEt.getText().toString());
                owner.setRegistrationNumber(ownerRegistrationNumber.getText().toString());

                car.setCarOwner(owner);
                car.setCarBrand(carBrandEt.getText().toString());
                car.setCarModel(carModelEt.getText().toString());
                car.setCarBodyType(carBodyTypeEt.getText().toString());

                if (carFromDB != null) {
                    car.setId(carFromDB.getId());
                }
                createOrEditCarsRequest(car);
            }
        });
        return view;
    }

    //Validation logic
    private boolean valid() {
        boolean valid = true;
        if (carBrandEt.getText().length() < MIN_LENDTH) {
            valid = false;
            carBrandEt.setError(getResources().getString(R.string.invalid));
        }
        if (carModelEt.getText().length() < MIN_LENDTH) {
            valid = false;
            carModelEt.setError(getResources().getString(R.string.invalid));
        }
        if (carBodyTypeEt.getText().length() < MIN_LENDTH) {
            valid = false;
            carBodyTypeEt.setError(getResources().getString(R.string.invalid));
        }
        if (ownerFirstNameEt.getText().length() < MIN_LENDTH) {
            valid = false;
            ownerFirstNameEt.setError(getResources().getString(R.string.invalid));
        }
        if (ownerLastNameEt.getText().length() < MIN_LENDTH) {
            valid = false;
            ownerLastNameEt.setError(getResources().getString(R.string.invalid));
        }
        if (ownerRegistrationNumber.getText().length() < MIN_LENDTH) {
            valid = false;
            ownerRegistrationNumber.setError(getResources().getString(R.string.invalid));
        }
        return valid;
    }

    //Create/edit car request
    private void createOrEditCarsRequest(Car mCar) {
        Observable<Car> car = RestClient.get().createOrEditCarData(mCar);
        carSubscription = car
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cars -> {
                    onCarSavedListener.onCarSaved(cars);
                    getFragmentStack().pop();
                });
    }
    //Callback to activity
    public interface OnCarSavedListener {
        void onCarSaved(Car car);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onCarSavedListener = (OnCarSavedListener) activity;
    }

    //Dispose to avoid memory leaks
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (carSubscription != null && !carSubscription.isDisposed()) {
            carSubscription.dispose();
        }
    }
}

