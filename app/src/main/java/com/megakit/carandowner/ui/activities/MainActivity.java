/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.megakit.carandowner.ui.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.megakit.carandowner.R;
import com.megakit.carandowner.model.entities.Car;
import com.megakit.carandowner.ui.BaseActivity;
import com.megakit.carandowner.ui.FragmentsStack;
import com.megakit.carandowner.ui.fragments.CarsListFragment;
import com.megakit.carandowner.ui.fragments.CreateCarFragment;
import com.megakit.carandowner.utils.CommonUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements CreateCarFragment.OnCarSavedListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fragment_container)
    FrameLayout fragmentContainer;
    @BindView(R.id.toolbar_title_tv)
    TextView titleTextView;

    private FragmentsStack fragmentsStack;
    private boolean mToolBarNavigationListener = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        fragmentsStack = new FragmentsStack(getFragmentManager(), R.id.fragment_container);
        if (savedInstanceState == null) {
            fragmentsStack.replace(CarsListFragment.newInstance());
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        changeToolbarTitle(getString(R.string.app_name));
        fragmentsStack.getFragmentManager().addOnBackStackChangedListener(this::shouldDisplayHomeUp);
        shouldDisplayHomeUp();

    }

    @Override
    public FragmentsStack getFragmentsStack() {
        return fragmentsStack;
    }

    //Update cars list fragment from create/edit fragment
    @Override
    public void onCarSaved(Car car) {
        CarsListFragment fragment = (CarsListFragment) getFragmentsStack().getFragmentManager().findFragmentByTag(CarsListFragment.class.getSimpleName());
        if (fragment != null) {
            fragment.updateCar(car);
        }
    }

    //Check and show navigate back button depending on fragments stack count
    public void shouldDisplayHomeUp() {
        boolean isHaveFragmentsStack = getFragmentManager().getBackStackEntryCount() > 0;
        if (isHaveFragmentsStack) {
            toolbar.setNavigationIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
            if (!mToolBarNavigationListener) {
                toolbar.setNavigationOnClickListener(v -> {
                    CommonUtils.hideKeyboard(MainActivity.this);
                    onBackPressed();
                });
                mToolBarNavigationListener = true;
            }
        } else {
            toolbar.setNavigationIcon(null);
            toolbar.setNavigationOnClickListener(null);
            mToolBarNavigationListener = false;
        }
    }

    //Change toolbar title text
    public void changeToolbarTitle(String title) {
        titleTextView.setText(title);
    }
}
