/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.megakit.carandowner.network;

import android.os.SystemClock;

import com.megakit.carandowner.model.DatabaseHelper;
import com.megakit.carandowner.model.entities.Car;
import com.megakit.carandowner.model.entities.Owner;
import com.megakit.carandowner.preference.AppPreference;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Anfisa on 11.08.2017.
 */
//Fake rest client class.
public class RestClient {
    private static RestClient instance;

    private RestClient() {
    }

    //Get instance of fake rest client.
    public static RestClient get() {
        if (instance == null) {
            instance = new RestClient();
        }
        return instance;
    }

    //Method to simulate request to get cars list.
    public Observable<List<Car>> getCarsListData() {
        //Check is application launcher for the first time.
        if (AppPreference.getInstance().getIsFirstLaunch()) {
            AppPreference.getInstance().setIsFirstLaunch(false);
            return getCarsListMockData();
        } else {
            return getCarsListFromDB();
        }
    }

    //Simulate request to get initial custom list.
    private Observable<List<Car>> getCarsListMockData() {
        Observable<List<Car>> carsObservable =
                Observable.fromCallable(this::getCarsList);

        return carsObservable
                .observeOn(Schedulers.computation())
                .map(cars -> {
                    DatabaseHelper.get().getCarDao().saveAll(cars);
                    return DatabaseHelper.get().getCarDao().getAll();
                });
    }

    //Simulate request to get current with latest changes cars and owners list.
    private Observable<List<Car>> getCarsListFromDB() {
        Observable<List<Car>> carsObservable =
                Observable.fromCallable(() -> {
                    SystemClock.sleep(1000);
                    return DatabaseHelper.get().getCarDao().getAll();
                });

        return carsObservable
                .observeOn(Schedulers.computation())
                .map(cars -> {
                    DatabaseHelper.get().getCarDao().saveAll(cars);
                    return DatabaseHelper.get().getCarDao().getAll();
                });
    }

    private List<Car> getCarsList() {
        SystemClock.sleep(1000);
        return createCarsList();
    }

    //Simulate request to delete car and owner objects.
    public Observable<Integer> deleteCarId(Integer carId, Integer ownerId) {
        Observable<Integer> observable =
                Observable.fromCallable(() -> {
                    SystemClock.sleep(1000);
                    return carId;
                });
        return observable
                .observeOn(Schedulers.computation())
                .map(carResponseId -> {
                    DatabaseHelper.get().getCarDao().deleteById(carResponseId);
                    DatabaseHelper.get().getOwnerDao().deleteById(ownerId);
                    return carResponseId;
                });
    }

    //Simulate request to create or edit car and owner object
    public Observable<Car> createOrEditCarData(Car car) {
        Observable<Car> observable =
                Observable.fromCallable(() -> {
                    SystemClock.sleep(1000);
                    return car;
                });
        return observable
                .observeOn(Schedulers.computation())
                .map(carResponse -> {
                    DatabaseHelper.get().getCarDao().save(carResponse);
                    return DatabaseHelper.get().getCarDao().getById(carResponse.getId());
                });
    }


    private ArrayList<Car> createCarsList() {
        ArrayList<Car> carArrayList = new ArrayList<>();
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Car car4 = new Car();
        Car car5 = new Car();

        Owner owner1 = new Owner();
        Owner owner2 = new Owner();
        Owner owner3 = new Owner();
        Owner owner4 = new Owner();
        Owner owner5 = new Owner();

        owner1.setFirstName("Leonardo");
        owner1.setSecondName("DiCaprio");
        owner1.setRegistrationNumber("123456");

        owner2.setFirstName("Natalie");
        owner2.setSecondName("Portman");
        owner2.setRegistrationNumber("234567");

        owner3.setFirstName("Luc");
        owner3.setSecondName("Besson");
        owner3.setRegistrationNumber("345678");

        owner4.setFirstName("Android");
        owner4.setSecondName("Droid");
        owner4.setRegistrationNumber("456789");

        owner5.setFirstName("Java");
        owner5.setSecondName("Avaj");
        owner5.setRegistrationNumber("456789");

        car1.setCarBodyType("Hatchback");
        car1.setCarBrand("Nissan");
        car1.setCarModel("Leaf");
        car1.setCarOwner(owner1);

        car2.setCarBodyType("Sedan");
        car2.setCarBrand("Toyota");
        car2.setCarModel("Camry");
        car2.setCarOwner(owner2);

        car3.setCarBodyType("Limousine");
        car3.setCarBrand("Hummer");
        car3.setCarModel("H2");
        car3.setCarOwner(owner3);

        car4.setCarBodyType("Station wagon");
        car4.setCarBrand("Ford");
        car4.setCarModel("Focus ST Wagon");
        car4.setCarOwner(owner4);

        car5.setCarBodyType("Coupe");
        car5.setCarBrand("Mini");
        car5.setCarModel("3 Door");
        car5.setCarOwner(owner5);
        carArrayList.add(car1);
        carArrayList.add(car2);
        carArrayList.add(car3);
        carArrayList.add(car4);
        carArrayList.add(car5);

        return carArrayList;
    }

}
