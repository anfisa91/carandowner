/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.megakit.carandowner.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.megakit.carandowner.R;
import com.megakit.carandowner.model.entities.Car;
import com.megakit.carandowner.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Anfisa on 11.08.2017.
 */

public class CarsAdapter extends RecyclerView.Adapter<CarsAdapter.ViewHolder> {

    private List<Car> carsList = new ArrayList<>();
    private CarsAdapter.OnItemClickListener listener;


    public CarsAdapter(List<Car> newsModelEntities) {
        this.carsList = newsModelEntities;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_car, viewGroup, false);
        CarsAdapter.ViewHolder mainHolder = new CarsAdapter.ViewHolder(v);
        v.setOnClickListener(view -> onItemClicked(mainHolder.getAdapterPosition()));
        return mainHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        //Display car and owner data in the list item view.
        final Car mCar = carsList.get(i);
        String carTextValue = mCar.getCarBrand() + " " + mCar.getCarModel() + " " + mCar.getCarBodyType();
        viewHolder.carData.setText(carTextValue);
        String ownerTextValue = "";
        if (mCar.getCarOwner() != null) {
            ownerTextValue = mCar.getCarOwner().getFirstName() + " " + mCar.getCarOwner().getSecondName() + " " + mCar.getCarOwner().getRegistrationNumber();
        }
        viewHolder.ownerData.setText(ownerTextValue);
        viewHolder.deleteCar.setOnClickListener(view -> onDeleteItemClicked(viewHolder.getAdapterPosition()));

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_car_data)
        TextView carData;
        @BindView(R.id.item_owner_data)
        TextView ownerData;
        @BindView(R.id.item_car_delete)
        ImageView deleteCar;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setListener(CarsAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return CommonUtils.isListEmpty(carsList) ? 0 : carsList.size();
    }

    private void onItemClicked(int adapterPosition) {
        if (listener != null) {
            listener.onItemClicked(adapterPosition);
        }
    }

    private void onDeleteItemClicked(int adapterPosition) {
        if (listener != null) {
            listener.onDeleteItemClicked(adapterPosition);
        }
    }

    //Interface to notify fragment about item click and delete icon click events.
    public interface OnItemClickListener {
        void onItemClicked(int position);

        void onDeleteItemClicked(int position);
    }


}
