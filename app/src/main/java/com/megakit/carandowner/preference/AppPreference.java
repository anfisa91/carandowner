/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.megakit.carandowner.preference;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Anfisa on 11.08.2017.
 */

public class AppPreference {
    private static final String PREF_NAME = "app_preferences";
    //Field to check is app launched for the first time
    private static final String IS_FIRST_LAUNCH = "is_first_launch";

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private static AppPreference sInstance;

    @SuppressLint("CommitPrefEdits")
    private AppPreference(Context context) {
        mSharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new AppPreference(context);
        }
    }

    //Check for app preference instance initialized.
    public static synchronized AppPreference getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(AppPreference.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }
        return sInstance;
    }

    public boolean getIsFirstLaunch() {
        return mSharedPreferences.getBoolean(IS_FIRST_LAUNCH, true);
    }

    public void setIsFirstLaunch(boolean isFirstLaunch) {
        mEditor.putBoolean(IS_FIRST_LAUNCH, isFirstLaunch);
        mEditor.commit();
    }
}
