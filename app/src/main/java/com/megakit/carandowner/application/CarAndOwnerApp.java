/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.megakit.carandowner.application;

import android.app.Application;

import com.megakit.carandowner.model.DatabaseHelper;
import com.megakit.carandowner.preference.AppPreference;

/**
 * Created by Anfisa on 10.08.2017.
 */

public class CarAndOwnerApp extends Application {
    public static CarAndOwnerApp instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        //Create instance for app shared preferences.
        AppPreference.initializeInstance(instance);
    }

    @Override
    public void onTerminate() {
        //Release database helper.
        DatabaseHelper.get().releaseHelper();
        super.onTerminate();

    }
}
